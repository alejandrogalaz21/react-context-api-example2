import React, { Component } from 'react';
// Context lets us pass a value deep into the component tree
// without explicitly threading it through every component.
// Create a context for the current theme (with "light" as the default).
const ThemeContext = React.createContext()

class App extends React.Component {
  state = {
    name: 'ROCKY'
  }

  changeName = name => this.setState({ name })
  onChange = e => this.setState({ name: e.target.value })
  render() {
    // Use a Provider to pass the current theme to the tree below.
    // Any component can read it, no matter how deep it is.
    // In this example, we're passing "dark" as the current value.
    return (
      <ThemeContext.Provider value={{ ...this.state, ...this }}>
        <Toolbar />
      </ThemeContext.Provider>
    );
  }
}

// A component in the middle doesn't have to
// pass the theme down explicitly anymore.
function Toolbar(props) {
  return (
    <div>
      <ThemedButton />
    </div>
  );
}

class ThemedButton extends Component {
  // Assign a contextType to read the current theme context.
  // React will find the closest theme Provider above and use its value.
  // In this example, the current theme is "dark".
  static contextType = ThemeContext;

  componentDidMount() {
    console.log(this.context)
  }

  handleChange = event => {
    this.context.changeName(event.target.value)
  }

  render() {
    return (
      <div>
        <button>{this.context.name}</button>
        <input onChange={this.context.onChange} />
        <input onChange={this.handleChange} />
      </div>

    )
  }
}

export default App;